package inno.maslakov;

/**
 * Created by sa on 14.03.17.
 */
public class Main {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Consumer());
        thread1.start();

        Thread thread2 = new Thread((new Producer()));
        thread2.start();

        Chat chat = new Chat();
        chat.chatter ("vasa2", "1234");
    }
}
