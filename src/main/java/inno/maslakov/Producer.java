package inno.maslakov;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by sa on 14.03.17.
 */
public class Producer implements Runnable{
    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory
                ("vm://localhost");
        try {
            Connection myConnection = factory.createConnection();
            myConnection.start();
            Session session = myConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("Dest");
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT); //важно,
            // иначе будет кэширование
            TextMessage textMessage = session.createTextMessage("Hello world");
            producer.send(textMessage);
            session.close();
            myConnection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}

