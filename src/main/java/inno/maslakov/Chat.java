package inno.maslakov;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;

import javax.jms.*;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by sa on 14.03.17.
 */
public class Chat implements MessageListener {

    public void chatter(String username, String password){
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory
                (username, password, "tcp://localhost:61616");

        RedeliveryPolicy queuePolicy = new RedeliveryPolicy();
        queuePolicy.setInitialRedeliveryDelay(0);
        queuePolicy.setRedeliveryDelay(1000);
        queuePolicy.setUseExponentialBackOff(false);
        queuePolicy.setMaximumRedeliveries(2);


        factory.setRedeliveryPolicy(queuePolicy);
        Session pubSession = null;
        Session subSession = null;
        MessageProducer publisher = null;
        Connection connect = null;
        try{
            connect = factory.createConnection (username, password);
            pubSession = connect.createSession(false, Session.AUTO_ACKNOWLEDGE);
            subSession = connect.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            e.printStackTrace();
        }

        try {
            Topic topic = pubSession.createTopic ("chat");
            MessageConsumer subscriber = subSession.createConsumer(topic);
            subscriber.setMessageListener(this);
            publisher = pubSession.createProducer(topic);
            connect.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader stdin =
                    new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
            System.out.println("\nChat application:\n"
                    + "=================\n"
                    + "The application user " + username);

            while (true) {
                String s = stdin.readLine();

                if (s.length() > 0) {
                    javax.jms.TextMessage msg = pubSession.createTextMessage();
                    msg.setText(username + ": " + s);
                    publisher.send(msg);
                }
            }
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onMessage(Message message) {
        try {
            System.out.println("before: " + message.getJMSRedelivered());
            TextMessage textMessage = (javax.jms.TextMessage)message;

            String string = null;
            string = textMessage.getText();
            message.acknowledge();
            System.out.println("after: " + message.getJMSRedelivered());
            System.out.println( string );
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
