package inno.maslakov;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class Consumer implements Runnable{

    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory
                ("vm://localhost");
        try {
            Connection connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("Dest");
            MessageConsumer consumer = session.createConsumer(destination);
            Message message = consumer.receive(10000);
            System.out.println(((TextMessage)message).getText());
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
